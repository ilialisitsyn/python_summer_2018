# https://projecteuler.net/problem=36
#
# The decimal number, 585 = 1001001001 in binary, is palindromic in both bases.
# Find the sum of all numbers, less than one million, which are palindromic in
# base 10 and base 2. (Please note that the palindromic number,
# in either base, may not include leading zeros.)
#
# Напишите программу, которая решает описанную выше задачу и печатает ответ.


def is_palindrome(input_str: str) -> bool:
    len_s = len(input_str)
    iter_num = len_s // 2
    r = True
    for i in range(iter_num):
        if input_str[i] != input_str[len_s - i - 1]:
            r = False
    return r


def sum_of_bin_dec_palindrome() -> int:
    r = 0
    for i in range(1000000 + 1):
        if is_palindrome(str(i)) and is_palindrome(bin(i)[2:]):
            r += i
    return r


if __name__ == '__main__':
    print(sum_of_bin_dec_palindrome())
