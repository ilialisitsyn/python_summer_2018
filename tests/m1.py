import modules.m1_Types_Data_Structures.hw1 as hw1
import modules.m1_Types_Data_Structures.hw2 as hw2
import modules.m1_Types_Data_Structures.hw3 as hw3
import modules.m1_Types_Data_Structures.hw4 as hw4
import modules.m1_Types_Data_Structures.hw5 as hw5


def test_hw1_remove_duplicates():
    if hw1.remove_duplicates(
            "asdfdsf324 ?3 efref4r4 23r(*&^*& efref4r4 a a bb ?3") == \
            "asdfdsf324 ?3 efref4r4 23r(*&^*& a bb":
        print("test_hw1_remove_duplicates is OK!")
    else:
        print("test_hw1_remove_duplicates is NOT OK!")


def test_hw2_most_frequent_1():
    output = hw2.most_frequent("собака кот кошка Собака").split(' ')
    if "собака" in output and len(output) == 1:
        print("test_hw2_most_frequent_1 is OK!")
    else:
        print("test_hw2_most_frequent_1 is NOT OK!")


def test_hw2_most_frequent_2():
    output = hw2.most_frequent("собака кот кошка Собака кот").split(' ')
    if "собака" in output and "кот" in output and len(output) == 2:
        print("test_hw2_most_frequent_2 is OK!")
    else:
        print("test_hw2_most_frequent_2 is NOT OK!")


def test_hw3_summ_of_str():
    if hw3.summ_of_str(
            "123dfgdr%0&45ty-45--900") == \
            -777:
        print("test_hw3_summ_of_str is OK!")
    else:
        print("test_hw3_summ_of_str is NOT OK!")


def test_hw4_lowest_from_string():
    if hw4.lowest_from_string(
            "2 1 8 4 2 3 5 7 10 18 82 2") == \
            6:
        print("test_hw4_lowest_from_string is OK!")
    else:
        print("test_hw4_lowest_from_string is NOT OK!")


def test_hw5_is_palindrome_1():
    if hw5.is_palindrome("101"):
        print("test_hw5_is_palindrome_1 is OK!")
    else:
        print("test_hw5_is_palindrome_1 is NOT OK!")


def test_hw5_is_palindrome_2():
    if hw5.is_palindrome("1001"):
        print("test_hw5_is_palindrome_2 is OK!")
    else:
        print("test_hw5_is_palindrome_2 is NOT OK!")


def test_hw5_is_palindrome_3():
    if not hw5.is_palindrome("1011"):
        print("test_hw5_is_palindrome_3 is OK!")
    else:
        print("test_hw5_is_palindrome_3 is NOT OK!")


def test_hw5_sum_of_bin_dec_palindrome():
    if hw5.sum_of_bin_dec_palindrome() == 872187:
        print("test_hw5_sum_of_bin_dec_palindrome is OK!")
    else:
        print("test_hw5_sum_of_bin_dec_palindrome is NOT OK!")


def run_all():
    test_hw1_remove_duplicates()
    test_hw2_most_frequent_1()
    test_hw2_most_frequent_2()
    test_hw3_summ_of_str()
    test_hw4_lowest_from_string()
    test_hw5_is_palindrome_1()
    test_hw5_is_palindrome_2()
    test_hw5_is_palindrome_3()
    test_hw5_sum_of_bin_dec_palindrome()


if __name__ == '__main__':
    run_all()
